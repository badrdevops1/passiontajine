FROM python:3.8.0-slim
WORKDIR /main
ADD . /main
RUN pip install --trusted-host pypi.python.org Flask
ENV NAME BADR
CMD ["python", "main.py"]
