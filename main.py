from flask import Flask
import os

app = Flask(__name__)

@app.route("/")
def skill():
    message = " Hi {name}! my instagram account is @passiontajine"
    return message.format(name=os.getenv("NAME", "Badr"))
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)